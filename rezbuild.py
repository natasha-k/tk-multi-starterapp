#!/usr/bin/env python

import os
import os.path
import shutil
import sys


def build(source_path, build_path, install_path, targets):

    def _copy_tree(src, dest):
        src_py = os.path.join(src, "tk-app")
        dest_py = os.path.join(dest, "tk-app")

        if os.path.exists(dest_py):
            shutil.rmtree(dest_py)
        shutil.copytree(src_py, dest_py)

    # Build
    _copy_tree(source_path, build_path)

    # Install
    if "install" in (targets or []):
        _copy_tree(build_path, install_path)


if __name__ == '__main__':
    build(
        source_path=os.environ['REZ_BUILD_SOURCE_PATH'],
        build_path=os.environ['REZ_BUILD_PATH'],
        install_path=os.environ['REZ_BUILD_INSTALL_PATH'],
        targets=sys.argv[1:]
    )
