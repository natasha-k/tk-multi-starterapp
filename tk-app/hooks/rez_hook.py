from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import os
import subprocess
import sys

import sgtk

HookBaseClass = sgtk.get_hook_baseclass()


class RezHook(HookBaseClass):
    """Hook to run an application."""

    def execute(self, app_path, app_args, version, **kwargs):
        """Start the required application using rez if required.

        Notes:
            - Define variables used to bootstrap tank from overwrite on
              first reference
            - Define others within ``tk-multi-launchapp.yml`` file in the
              ``extra:rez:parent_variables`` list.

        Args:
            app_path (str):
                The path of the application executable
            app_args (str):
                Any arguments the application may require
            version (str):
                version of the application being run if set in the "versions"
                settings of the Launcher instance, otherwise ``None``

        Returns:
            dict[str]:
                Execute results mapped to 'command' (str) and
                'return_code' (int).
        """
        app = self.parent
        rez_info = app.get_setting("extra", {}).get("rez", {})

        # Execute App in a Rez context
        rez_py_path = self.get_rez_path()
        if rez_py_path:
            if rez_py_path not in sys.path:
                self.logger.debug('Appending to sys.path: "%s"', rez_py_path)
                sys.path.append(rez_py_path)

            # Only import after rez_py_path is inside sys.path
            from rez.resolved_context import ResolvedContext
            from rez.config import config

            rez_parent_variables = rez_info.get("parent_variables", [])
            config.parent_variables = rez_parent_variables
            rez_packages = [
                request.format(version=version)
                for request in rez_info.get("packages", [])
            ]

            context = ResolvedContext(rez_packages)
            context.apply()

            self.logger.error(sys.path)

        return context

    def get_rez_path(self, strict=True):
        """Get ``rez`` python package path from the current environment.

        Args:
            strict (bool):
                Whether to raise an error if Rez is not available as a package.
                This will prevent the app from being launched.

        Returns:
            str: A path to the Rez package, can be empty if ``strict=False``..
        """
        rez_cmd = 'C:\\opt\\rez\\Scripts\\rez\\rez-python.exe -c "import rez; print(rez.__path__[0])"'
        process = subprocess.Popen(rez_cmd, stdout=subprocess.PIPE, shell=True)
        rez_python_path, err = process.communicate()

        if err or not rez_python_path:
            if strict:
                raise ImportError(
                    "Failed to find Rez as a package in the current "
                    "environment! Try 'rez-bind rez'!"
                )
            else:
                self.logger.warn(
                    "Failed to find a Rez package in the current "
                    "environment. Unable to request Rez packages."
                )

            rez_python_path = ""
        else:
            absolute_path = os.path.abspath(rez_python_path.strip())
            rez_python_path = os.path.dirname(absolute_path)
            self.logger.debug("Found Rez in: %s", rez_python_path)

        return rez_python_path