name = "tk_multi_starterapp"

version = "0.1.0"

authors = [
    "n.kelkar"
]

description = \
    """
    Shotgun toolkit starter app.
    """

requires = [
    "python",
    "jfLogging"
]

build_command = 'python {root}/rezbuild.py {install}'
